# Copyright Vincent LAMBERT (2014)
#
# vincent@influence-pc.fr
#
# This software is a computer program whose purpose is to control a robot from a web interface. It's designed to be hosted by a microcomputer connected to an Arduino board.
#
# This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty  and the software's author,  the holder of the economic rights, and the successive licensors  have only  limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had knowledge of the CeCILL-B license and that you accept its terms.

# Bottle.py global variable
webserverPort = 8080
lastHTTPrequest = 0

# Email informations
mailFrom = ""
mailTo = ""
mailUsername = ""
mailPassword = ""
mailStartTLS = True # True or None
mailSMTPserver = ""
mailSMTPport = ""

# Arduino conf
maxServoAngle = 120 # /255

# Javascript related
maxJoystickValue = 100   # Same value as in views/gamepad.tpl
leftJoystickX = 0       # Unused
leftJoystickY = 0       # Used for acceleration
rightJoystickX = 0      # Used for direction
rightJoystickY = 0      # Used to change the webcam angle
