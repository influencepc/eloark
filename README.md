# Eloark
## [not already] Autonomous multitasks robot

### Arduino installation (optional for test purpose)

- Download and unzip [Arduino 1.5+](http://arduino.cc/en/Main/Software into dependencies/)
- Unzip `dependencies/nanpy-firmware-master.zip`
- Launch `dependencies/nanpy-firmware-master/configure.sh`
- Copy `dependencies/nanpy-firmware-master/Nanpy` into `dependencies/arduino-1.5.7/examples`
- Launch `dependencies/arduino-1.5.7/arduino`
- Go in `Files/Examples/Nanpy` and download it to your board (if necessary, set your model in the Tools menu)

### Setup the embedded computer

- Launch as root:

        apt-get install python-setuptools python-bottle python-opencv python-redis redis-server

- Unzip `dependencies/nanpy-master.zip`
- Launch as root:

        python dependencies/nanpy-master/setup.py install

### Script usage

In my opinion, the script should be launched at startup. My favourite way is to use a Crontab "@reboot".

The script will send you a mail at the address written in the `globalConfig.py` file, containing a link to control the robot.

### Legal concern

You are about to download a free software subject to the [CeCILL-B license](http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html). You acknowledge having read this license, and have accepted the terms. This software is a computer program used to control a robot from a web interface. It's designed to be hosted by a microcomputer connected to an Arduino board.

![Eloark](http://influence-pc.fr/envois/IMG_00041.jpg)
