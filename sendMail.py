# Copyright Vincent LAMBERT (2014)
#
# vincent@influence-pc.fr
#
# This software is a computer program whose purpose is to control a robot from a web interface. It's designed to be hosted by a microcomputer connected to an Arduino board.
#
# This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty  and the software's author,  the holder of the economic rights, and the successive licensors  have only  limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had knowledge of the CeCILL-B license and that you accept its terms.

import socket, fcntl, struct, smtplib, threading
import globalConfig

def get_ip_address(ifname):
    socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(socket.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])

def sendEmail():
  try:
    server = smtplib.SMTP(globalConfig.mailSMTPserver, globalConfig.mailSMTPport)
    if globalConfig.mailStartTLS:
      server.starttls()
    server.login(globalConfig.mailUsername, globalConfig.mailPassword)
    header = "To:" + globalConfig.mailTo + "\n" + "From: " + globalConfig.mailFrom + "\n" + "Subject:Now at " + get_ip_address("wlan0") + "\n\n"
    content = "Take control of the robot at http://" + get_ip_address("wlan0") + ":" + str(globalConfig.webserverPort) + "\n\n"
    server.sendmail(globalConfig.mailFrom, globalConfig.mailTo, header+content)
    server.quit()
    print("Mail sent to " + globalConfig.mailTo)
  except:
    print("The mail can't be sent")

mailThread = threading.Thread(None, sendEmail)
mailThread.start()
mailThread._Thread__stop()
