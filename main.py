#!/usr/bin/python -O
# -*- coding: utf-8 -*-
#
# Copyright Vincent LAMBERT (2014)
#
# vincent@influence-pc.fr
#
# This software is a computer program whose purpose is to control a robot from a web interface. It's designed to be hosted by a microcomputer connected to an Arduino board.
#
# This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty  and the software's author,  the holder of the economic rights, and the successive licensors  have only  limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had knowledge of the CeCILL-B license and that you accept its terms.

import redis, random, threading, time
import globalConfig, robotControl, sendMail, videoProcessing
from bottle import route, run, request, response, redirect, template, static_file

db = redis.Redis('localhost')

@route('/assets/<filepath:path>')
def assets(filepath):
  return static_file(filepath, root='assets/')

@route('/')
def index():
  user = "nobody"
  if request.get_cookie("session") and db.get("session") == request.get_cookie("session"):
    user = "the one"
  elif db.exists("session"):
    user = "a guest"
  return template('index', serverIP = sendMail.get_ip_address("wlan0"), user = user)

@route('/controls/<stick1x>/<stick1y>/<stick2x>/<stick2y>')
def controls(stick1x, stick1y, stick2x, stick2y):
  if request.get_cookie("session") and db.get("session") == request.get_cookie("session"):
    globalConfig.leftJoystickX = int(stick1x)
    globalConfig.leftJoystickY = int(stick1y)
    globalConfig.rightJoystickX = int(stick2x)
    globalConfig.rightJoystickY = int(stick2y)
    globalConfig.lastHTTPrequest = time.time()
    # Session lifetime, resetted once every 10s
    if db.ttl("session") < 50:
      db.expire("session", 60)
  return 'stick1x = ' + stick1x + " stick1y = " + stick1y + " stick2x = " + stick2x + " stick2y = " + stick2y

@route('/login')
def login():
  if not db.exists("session"):
    random_value = str(random.getrandbits(128))
    db.set("session", random_value)
    db.expire("session", 60)
    db.bgsave()
    response.set_cookie("session", random_value)

    ledThread = threading.Thread(None, robotControl.toggleLED)
    ledThread.start()
    ledThread._Thread__stop()
    return 'connected'
  elif request.get_cookie("session") and db.get("session") == request.get_cookie("session"):
    return 'connected'
  else:
    return 'waiting'

@route('/logout')
def logout():
  if request.get_cookie("session") and db.get("session") == request.get_cookie("session"):
    db.delete("session")
    db.bgsave()
    response.set_cookie("session", "")
  return 'offline'

@route('/status')
def status():
  if not db.exists("session"):
    return 'offline'
  elif request.get_cookie("session") and db.get("session") == request.get_cookie("session"):
    return 'connected'
  else:
    return 'waiting'

run(host='0.0.0.0', port=globalConfig.webserverPort, debug=True)
