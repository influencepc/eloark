# Copyright Vincent LAMBERT (2014)
#
# vincent@influence-pc.fr
#
# This software is a computer program whose purpose is to control a robot from a web interface. It's designed to be hosted by a microcomputer connected to an Arduino board.
#
# This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty  and the software's author,  the holder of the economic rights, and the successive licensors  have only  limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had knowledge of the CeCILL-B license and that you accept its terms.

import cv2, subprocess, sys

process = subprocess.Popen( ["cvlc", "v4l2:///dev/video0", "--sout", "#transcode{vcodec=MJPG,vb=500,width=352,height=288}:duplicate{dst=std{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9090/webcam.mjpg}}"]
                          )

print("Video capture started")





# videoProcess = None
# count = 0

# def turnOnWebcam(counter):
#   videoProcess = subprocess.Popen(["cvlc", "v4l2:///dev/video" + str(counter), "--sout", "#transcode{vcodec=MJPG,vb=500,width=352,height=288}:duplicate{dst=std{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9090/webcam.mjpg}}"], stdout=subprocess.PIPE)

#   print("Starting video capture on /dev/video" + str(counter))

#   for line in videoProcess.stdout:
#     print(videoProcess.stdout.read())
#     if "v4l2 demux error" in line:
#       videoProcess.kill()
#       count += 1
#       turnOnWebcam(count)
#       break
#     elif count == 2:
#       videoProcess.kill()
#       print("Video capture can't be started")
#       break

# turnOnWebcam(count)





# with open("test.avi", "rb") as infile:
#     p = subprocess.Popen(["ffmpeg", "-i", "-", "-f", "matroska", "-vcodec", "mpeg4", "-strict", "experimental", "-"], stdin=infile, stdout=subprocess.PIPE)
#     while True:
#         data = p.stdout.read(1024)
#         if len(data) == 0:
#             break
#         # do something with data...
#         print(data)
#     print p.wait() # should have finisted anyway

# webcam = cv2.VideoCapture(0)
# while (webcam.isOpened()):
#     #read() return a tuple (True/False, frame/None)
#     returnValue, frame = webcam.read()
#     if (returnValue):
#         sys.stdout.write(frame.tostring())
#     else:
#         break
# webcam.release
