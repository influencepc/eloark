<!DOCTYPE html>
<html id="page">
  <head>
    <link rel="stylesheet" href="/assets/style.css">
    <script type="text/javascript" src="/assets/script.js"></script>
    <script type="text/javascript" src="/assets/virtualjoystick.js/virtualjoystick.js"></script>
    <meta name="viewport" content="user-scalable=no" />
    <title>Gamepad</title>
  </head>
  <body>
    <img id="videoFeed" src="http://{{serverIP}}:9090/webcam.mjpg">
    <!-- <video id="videoFeed" preload="auto" autoplay="autoplay">
      <source src="http://techslides.com/demos/sample-videos/small.mp4" type="video/mp4">
      <source src="http://techslides.com/demos/sample-videos/small.ogv" type="video/ogg">
      <source src="http://video.webmfiles.org/big-buck-bunny_trailer.webm" type="video/webm">
      Your browser does not support the video tag.
    </video> -->

    <div id="sticksContainer">
      <div id="stick1"></div><div id="stick2"></div>
    </div>

    <span class="btn" style="left: 0.2em" id="fullscreenIcon"><span id="startFullscreenIcon">↗</span><span id="leaveFullscreenIcon">↙</span></span>
    <span class="btn" style="right: 0.2em" id="accountIcon"><span id="loginIcon">▶</span><span id="logoutIcon">■</span><span id="waitIcon">◷</span></span>
    <!-- <span class="btn" style="right: 0.2em"><span id="startAutopilotIcon">⊕</span><span id="leaveAutopilotIcon">⊙</span></span> -->
  </body>
</html>

<script>maxJoystickValue = 100; // must be the same value as "maxJoystickValue" variable from globalConfig.py



  var page = document.getElementById("page");
  var startFullscreenIcon = document.getElementById("startFullscreenIcon");
  var leaveFullscreenIcon = document.getElementById("leaveFullscreenIcon");

  fullscreenIcon.onclick = function() { toggleFullscreenMode() };

  function toggleFullscreenMode() {
    if (document.fullscreenEnabled || document.webkitIsFullScreen || document.mozFullScreen) {
      exitFullscreen();
      startFullscreenIcon.style.display = "block";
      leaveFullscreenIcon.style.display = "none";
    }
    else {
      enterFullscreen(page);
      startFullscreenIcon.style.display = "none";
      leaveFullscreenIcon.style.display = "block";
    }
  }



  function ajaxRequest(url, callback = function(){}) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      callback(xhr);
    };
    xhr.open("GET", url, true);
    xhr.send(null);
  }



  var accountIcon = document.getElementById("accountIcon");
  var loginIcon = document.getElementById("loginIcon");
  var logoutIcon = document.getElementById("logoutIcon");
  var waitIcon = document.getElementById("waitIcon");

  currentVisibleIcon = loginIcon; // Initial CSS state
  function setCurrentVisibleIcon() {
    for (i = 0; i < accountIcon.children.length; i++) {
      // Works thanks to the initial CSS state
      if (accountIcon.children[i].style.display != "none") {
        currentVisibleIcon = accountIcon.children[i];
      }
    }
  }

  function updateStatus(xhr) {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
      console.log("You are " + xhr.responseText);

      // Hides the current icon
      for (i = 0; i < accountIcon.children.length; i++) {
        accountIcon.children[i].style.display = "none";
      }

      // Sets the new one
      switch(xhr.responseText) {
        case "connected":
          logoutIcon.style.display = "initial";
          break;
        case "waiting":
          waitIcon.style.display = "initial";
          break;
        case "offline":
          loginIcon.style.display = "initial";
          break;
      }
    }
    setCurrentVisibleIcon();
  }

  accountIcon.onclick = function() { updateAccountIcon() };
  function updateAccountIcon() {
    if (currentVisibleIcon.id == "logoutIcon") {
      ajaxRequest("/logout", updateStatus)
    }
    else {
      ajaxRequest("/login", updateStatus);
    }
  }

  // Sets initial status
  ajaxRequest("/status", updateStatus);
  setInterval(function() {
    ajaxRequest("/status", updateStatus);
  }, 5000);



  console.log("Touchscreen is", VirtualJoystick.touchScreenAvailable() ? "available" : "not available");

  var joystick1 = new VirtualJoystick({
    container : document.getElementById('stick1'),
    strokeStyle : 'cyan',
    mouseSupport  : true,
    limitStickTravel: true,
    stickRadius : maxJoystickValue
  });
  joystick1.addEventListener('touchStart', function() {
    console.log('down1');
  })
  joystick1.addEventListener('touchEnd', function() {
    console.log('up1');
  })

  var joystick2 = new VirtualJoystick({
    container : document.getElementById('stick2'),
    strokeStyle : 'orange',
    mouseSupport  : true,
    limitStickTravel: true,
    stickRadius : maxJoystickValue
  });
  joystick2.addEventListener('touchStart', function() {
    console.log('down2');
  })
  joystick2.addEventListener('touchEnd', function() {
    console.log('up2');
  })

  setInterval(function() {
    console.log('Stick1 = '
                + ' dx:' + joystick1.deltaX()
                + ' dy:' + joystick1.deltaY() * -1
                + (joystick1.right() ? ' right' : '')
                + (joystick1.up() ? ' up'   : '')
                + (joystick1.left() ? ' left' : '')
                + (joystick1.down() ? ' down' : '')
              );
    console.log('Stick2 = '
                + ' dx:' + joystick2.deltaX()
                + ' dy:' + joystick2.deltaY() * -1
                + (joystick2.right() ? ' right' : '')
                + (joystick2.up() ? ' up'   : '')
                + (joystick2.left() ? ' left' : '')
                + (joystick2.down() ? ' down' : '')
              );
    ajaxRequest("/controls/" + parseInt(joystick1.deltaX()) + "/" + parseInt(joystick1.deltaY() * -1) + "/" + parseInt(joystick2.deltaX()) + "/" + parseInt(joystick2.deltaY() * -1), function(xhr){})
  }, 100);
</script>
